/* Copyright (c) 2014 Jons-Tobias Wamhoff

 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

#ifndef JOQUER_TOPOLOGY_H_
#define JOQUER_TOPOLOGY_H_

#include <unordered_map>
#include <joquer/cpuid.hpp>
#include <joquer/sysfs.hpp>

namespace joquer {

class Topology {
    typedef enum {
        VendorAMD        = 0x000001,
        VendorIntel      = 0x000002,
        Model15h         = 0x000004,
        ModelSandyBridge = 0x000008,
        ModelIvyBridge   = 0x000010,
        ModelHaswell     = 0x000020
    } Specification;

    unsigned int spec;

    struct CpuTopology {
        int coreId;
        int packageId;
        std::vector<int> coreSiblings;
        std::vector<int> threadSiblings;
    };

    std::unordered_map<int, std::vector<int> > packages;
    std::unordered_map<int, CpuTopology> cores;
    std::vector<int> coresOnline;

    void retrieve() {
        coresOnline = SysFSBase::getCoresOnline();
        for (auto c : coresOnline) {
            cores[c].coreId = SysFSBase::getCoreId(c);
            cores[c].packageId = SysFSBase::getPackageId(c);
            cores[c].coreSiblings = SysFSBase::getCoreSiblings(c);
            cores[c].threadSiblings = SysFSBase::getThreadSiblings(c);

            packages[cores[c].packageId].push_back(c);
        }
    }
public:
    Topology() : spec(0) {
        if (CPUID::checkAMDFamily15h())
            spec = spec | Specification::VendorAMD | Specification::Model15h;
        else if (CPUID::checkIntelFamily6()) {
            spec |= Specification::VendorIntel;
            if (CPUID::isIntelSandyBridge())
                spec |= Specification::ModelSandyBridge;
            else if (CPUID::isIntelIvyBridge())
                spec |= Specification::ModelIvyBridge;
            else if (CPUID::isIntelHaswell())
                spec |= Specification::ModelHaswell;
        }

        retrieve();
    }

    bool isIntel() { return spec & Specification::VendorIntel; }

    bool isAMD() { return spec & Specification::VendorAMD; }

    bool isSandyBridge() { return spec & Specification::ModelSandyBridge; }

    bool isHaswell() { return (spec & Specification::ModelHaswell) || (spec & Specification::ModelIvyBridge) ; }

    std::vector<int> getPackageCores(int package) { return packages[package]; }

    int getCoreCount() { return cores.size(); }

    std::vector<int> getCoresOnline() { return coresOnline; }

    std::vector<int> getPackages() {
        std::vector<int> pack;
        pack.reserve(packages.size());

        for (auto p : packages)
            pack.push_back(p.first);

        return pack;
    }

    std::vector<int> getThreadSiblings(int core) { return cores[core].threadSiblings; }

    int coreMap(int threadId) { // TODO parameter for Topology::Strategy
        threadId = threadId % getCoreCount();
        return getPackageCores(threadId / getPackageCores(0).size()).at(threadId % getPackageCores(0).size());
    }
};

}

#endif /* JOQUER_TOPOLOGY_H_ */
