/* Copyright (c) 2014 Jons-Tobias Wamhoff

 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

#ifndef JOQUER_MSR_H_
#define JOQUER_MSR_H_

/*
 * Driver:      http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/tree/arch/x86/kernel/msr.c
 * Instruction: http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/tree/arch/x86/include/asm/msr.h
 * MSRs:        http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/tree/arch/x86/include/uapi/asm/msr-index.h
 */

#include <assert.h>
#include <asm/msr.h>
#include <fcntl.h>
#include <memory>
#include <stdint.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include <joquer/cpuid.hpp>
#include <joquer/driver.h>
#include <joquer/helper.hpp>
#include <joquer/sysfs.hpp>
#include <joquer/topology.hpp>

namespace joquer {

#ifndef MSR_AMD_PSTATE_LIM
# define MSR_AMD_PSTATE_LIM        0xC0010061
#else
# warning MSR_AMD_PSTATE_LIM already defined
#endif

#ifndef MSR_AMD_COFVID_STATUS
# define MSR_AMD_COFVID_STATUS     0xC0010071
#else
# warning MSR_AMD_COFVID_STATUS already defined
#endif

class MSR {
    int* msrFds;
    int  drvFd;
    std::shared_ptr<Topology> topo;

    uint64_t msrFileRead(int core, unsigned int offset) {
        assert(msrFds[core] != -1);
        uint64_t result = 0;
        
        int bytes = pread(msrFds[core], &result, sizeof(result), offset);
        Debug::checkErrorCode("MSR:msrFileRead:pread", bytes);
        Debug::checkReturnCode("MSR:msrFileRead:pread", bytes, sizeof(result));

        return result;
    }

    void msrFileWrite(int core, unsigned int offset, unsigned long value) {
        assert(msrFds[core] != -1);
        int bytes = pwrite(msrFds[core], &value, sizeof(value), offset);
        Debug::checkErrorCode("MSR:msrFileWrite:pwrite", bytes);
        Debug::checkReturnCode("MSR:msrFileWrite:pwrite", bytes, sizeof(value));
    }

    uint64_t msrIoctlGet(int core, unsigned int offset) {
        assert(msrFds[core] != -1);
        uint32_t regs[8] = {0};
        regs[1] = offset;
        int rc = ioctl(msrFds[core], X86_IOC_RDMSR_REGS, &regs);
        Debug::checkErrorCode("MSR:msrIoctlGet:ioctl", rc);
        return regs[0] | ((uint64_t)regs[2] << 32);
    }

    void msrIoctlSet(int core, unsigned int offset, unsigned long value) {
        assert(msrFds[core] != -1);
        uint32_t regs[8] = {0};
        regs[0] = (uint32_t)value;
        regs[1] = offset;
        regs[2] = value >> 32;
        int rc = ioctl(msrFds[core], X86_IOC_WRMSR_REGS, &regs);
        Debug::checkErrorCode("MSR:msrIoctlSet:ioctl", rc);
    }

    uint64_t trbIoctlGet(int cmd) {
        uint64_t res;
        int rc = ioctl(drvFd, cmd, &res);
        Debug::checkErrorCode("MSR:trbIoctlGet:ioctl", rc);
        if (cmd == TURBO_IOC_GET_PSTATE)
            return rc;
        else
            return res;
    }

    uint64_t trbIoctlSet(int cmd, uint64_t val) {
        int rc;
        if (cmd == TURBO_IOC_SET_PSTATE)
            rc = ioctl(drvFd, cmd, static_cast<unsigned int>(val));
        else
            rc = ioctl(drvFd, cmd, &val);
        Debug::checkErrorCode("MSR:trbIoctlSet:ioctl", rc);
        return val;
    }
public:
    MSR(std::shared_ptr<Topology> t = std::make_shared<Topology>()) : topo(t) {
        if (getuid() != 0)
            Debug::userError("MSR:MSR:no root privileges");
        if (!topo->isIntel() && !topo->isAMD())
            Debug::userError("MSR:MSR:CPU not supported");

        int coreCount = topo->getCoreCount();
        msrFds = new int[coreCount];
        std::fill_n(msrFds, coreCount, -1);

        for (auto i : topo->getCoresOnline()) {
            std::stringstream path;
            path << "/dev/cpu/" << i << "/msr";
            std::string pathStr(path.str());

            if (access(pathStr.c_str(), F_OK) == -1) {
                int rc = system("modprobe msr");
                if (!(WIFEXITED(rc) && WEXITSTATUS(rc) == 0))
                    Debug::userError("MSR:MSR:modprobe:msr not available");
            }

            int fd = open(pathStr.c_str(), O_RDWR); //O_RDONLY
            Debug::checkErrorCode("MSR:MSR:open:msr", fd);
            msrFds[i] = fd;
        }

        const char* drvPath = "/dev/turbo";
        if (access(drvPath, F_OK) != -1) {
            drvFd = open(drvPath, O_RDWR);
            Debug::checkErrorCode("MSR:MSR:open:turbo", drvFd);
        } else {
            drvFd = -1;
            Debug::userWarning("MSR:MSR:open:turbo no driver found");
        }
    }

    ~MSR() {
        // XXX between constructor and destructor, online cores may have changed.
        for (auto i : topo->getCoresOnline()) {
            close(msrFds[i]);
            msrFds[i] = -1;
        }

        delete[] msrFds;
        if (drvFd != -1)
            close(drvFd);
    }

    /*        AMD            both     Intel
     * TSC    P0sw           always   fixed-rate frequency of the processor
     * MPERF  P0sw           in C0    proportional to boot defined fixed frequency
     * APERF  actual cycles  in C0    proportional to actual performance
     */
    
    uint64_t getAPerf(int core = -1) {
        if (core == -1 && drvFd != -1)
            return trbIoctlGet(TURBO_IOC_GET_APERF);

        if (core == -1)
            core = Hardware::getCoreId();

        return msrFileRead(core, MSR_IA32_APERF); // MSR0000_00E8[APERF] or E8H IA32_APERF[C0_ACNT]
    }

    uint64_t getMPerf(int core = -1) {
        if (core == -1 && drvFd != -1)
            return trbIoctlGet(TURBO_IOC_GET_MPERF);

        if (core == -1)
            core = Hardware::getCoreId();

        return msrFileRead(core, MSR_IA32_MPERF); // MSR0000_00E7[MPERF] or E7H IA32_MPERF[C0_MCNT]
    }

    /*
     * COF          : Current operating frequency of a given clock domain
     * Frequency ID : multiplier applied to the PLL frequency to convert it into the COF
     * Halving ID   : bit enables or disables an additional 0.5 to the given FID (Intel only)
     * Divisor ID   : post-PLL power-of-two frequency divisor bit used to reduce the COF
     * Voltage ID   : signal to the voltage regulator (sent via a designated serial connection from the CPU to the external regulator, internal for Intel)

     * P-State Intel: FID multiplier based on units of 100MHz, all active cores run at same fastest requested freuency, turbo only if some are inactive
     * P-State AMD  : ACPI style P0 (fastest) to P7, each state defines its own FID, DID, VID in MSR_AMD_PSTATE_DEF_BASE
     */

    int getPState(int core = -1) {
        if (core == -1 && drvFd != -1)
            return trbIoctlGet(TURBO_IOC_GET_PSTATE);
        
        if (core == -1)
            core = Hardware::getCoreId();

        if (topo->isIntel())
            return Helper::getBits(msrFileRead(core, MSR_IA32_PERF_STATUS), 8, 15); // 198H MSR_PERF_STATUS[Current Performance State Value]
        else
            return Helper::getBits(msrFileRead(core, MSR_AMD_PERF_STATUS), 0, 2); // MSRC001_0063[CurPstate] ACPI style SW P0..P7
    }

    void setPState(uint64_t pState, int core = -1, int turboDis = 0) {
        if (core == -1 && turboDis == 0 && drvFd != -1)
            trbIoctlSet(TURBO_IOC_SET_PSTATE, pState);
        else {
            if (core == -1)
                core = Hardware::getCoreId();

            // Turbo can be also disable globally (see getBoostingPackage)
            if (topo->isIntel())
                msrFileWrite(core, MSR_IA32_PERF_CTL, Helper::setBits(turboDis ? Helper::setBits(0, 32, 32, 1) : 0, 8, 15, pState)); // 199H IA32_PERF_CTL[Target performance State Value] preserve IA32_PERF_CTL[IDA Disengage]
            else
                msrFileWrite(core, MSR_AMD_PERF_CTL, Helper::setBits(0, 0, 2, pState)); // MSRC001_0062[PstateCmd] ACPI style SW P0..P7
        }
    }

    bool getBoostingPackage(int package) {
        if (topo->isIntel())
            // Note that this a writable MSR and Turbo can globally be disabled here
            return ((msrFileRead(topo->getPackageCores(package)[0], MSR_IA32_MISC_ENABLE) & MSR_IA32_MISC_ENABLE_TURBO_DISABLE) == 0); // 1A0H IA32_MISC_ENABLE[IDA Disable]
        else
            return true;
    }

    void setBoostingPackage(int package) {
        if (topo->isIntel()) {
            uint64_t misc = msrFileRead(topo->getPackageCores(package)[0], MSR_IA32_MISC_ENABLE);
            if ((misc & MSR_IA32_MISC_ENABLE_TURBO_DISABLE) != 0)
                msrFileWrite(topo->getPackageCores(package)[0], MSR_IA32_MISC_ENABLE, misc & ~MSR_IA32_MISC_ENABLE_TURBO_DISABLE); // 1A0H IA32_MISC_ENABLE[IDA Disable]
        }
    }

    bool getBoostingCore(int core) {
        if (topo->isIntel())
            return (Helper::getBits(msrFileRead(core, MSR_IA32_PERF_CTL), 32, 32) == 0); // 199H IA32_PERF_CTL[IDA Disengage]
        else
            return (Helper::getBits(msrFileRead(core, MSR_K7_HWCR), 25, 25) == 0); // MSRC001_0015[CpbDis]
    }

    void setBoostingCore(int core, bool enable = true) {
        if (topo->isIntel()) {
            uint64_t pState = msrFileRead(core, MSR_IA32_PERF_CTL);
            if (Helper::getBits(pState, 32, 32) != (enable ? 0 : 1))
                msrFileWrite(core, MSR_IA32_PERF_CTL, Helper::setBits(pState, 32, 32, enable ? 0 : 1)); // 199H IA32_PERF_CTL[IDA Disengage]
        } else {
            uint64_t hwcr = msrFileRead(core, MSR_K7_HWCR);
            if (Helper::getBits(hwcr, 25, 25) != (enable ? 0 : 1))
                msrFileWrite(core, MSR_K7_HWCR, Helper::setBits(hwcr, 25, 25, enable ? 0 : 1)); // MSRC001_0015[CpbDis]
        }
    }

    int getPStateFast(int core) {
         if (topo->isIntel())
            return Helper::getBits(msrFileRead(core, MSR_NHM_TURBO_RATIO_LIMIT), 0, 7); // 1ADH MSR_TURBO_RATIO_LIMIT[Maximum Ratio Limit for 1C] maximum turbo ratio limit of 1 core active
        else 
            return 0; // AMD's MSRs always refer to SW P-state numbering
    }

    int getPStateBase(int core) {
        if (topo->isIntel())
            return Helper::getBits(msrFileRead(core, MSR_PLATFORM_INFO), 8, 15); // CEH MSR_PLATFORM_INFO[Maximum Non-Turbo Ratio] ratio of the frequency that invariant TSC runs at, unit of 100 MHz
        else
            return Helper::getBits(msrFileRead(core, MSR_AMD_PSTATE_LIM), 0, 2); // MSRC001_0061[CurPstateLimit] highest-performance non-boosted SW P-state
    }

    int getPStateSlow(int core) {
        if (topo->isIntel())
            if (topo->isHaswell()) {
                // 47:40 Maximum Efficiency Ratio (R/O) The is the minimum ratio (maximum efficiency) that the processor can operates, in units of 100MHz.
                return Helper::getBits(msrFileRead(core, MSR_PLATFORM_INFO), 48, 55); // CEH MSR_PLATFORM_INFO[Minimum Operating Ratio] minimum supported operating ratio in units of 100 MHz
            } else
                return Helper::getBits(msrFileRead(core, MSR_PLATFORM_INFO), 40, 47); // CEH MSR_PLATFORM_INFO[Maximum Efficiency Ratio] minimum ratio (maximum efficiency) that the processor can operates, in units of 100MHz
        else
            return Helper::getBits(msrFileRead(core, MSR_AMD_PSTATE_LIM), 4, 6); // MSRC001_0061[PstateMaxVal] lowest-performance non-boosted SW P-state
    }

    bool getMonMwaitUserEnable(int core) {
        if (!topo->isIntel())
            return Helper::getBits(msrFileRead(core, MSR_K7_HWCR), 10, 10); // MSRC001_0015[MonMwaitUserEn]
        else
            return false;
    }

    void setMonMwaitUserEnable(int core) {
        if (!topo->isIntel())
            msrFileWrite(core, MSR_K7_HWCR, Helper::setBits(Helper::setBits(msrFileRead(core, MSR_K7_HWCR), 9, 10, 2), 26, 26, 0)); // MSRC001_0015[MonMwaitUserEn=1, MonMwaitDis=0, EffFreqCntMwait=0]
    }

    int getNbPstateDis(int core) {
        if (!topo->isIntel())
            return Helper::getBits(msrFileRead(core, MSR_AMD_COFVID_STATUS), 23, 23); // MSRC001_0071[NbPstateDis]
        else
            return -11;
    }

    int getFid(int core) { 
        if (topo->isIntel())
            return Helper::getBits(msrFileRead(core, MSR_IA32_PERF_STATUS), 8, 15); // same as P-state
        else
            return Helper::getBits(msrFileRead(core, MSR_AMD_COFVID_STATUS), 0, 5); // MSRC001_0071[CurCpuFid]
    }

    int getDid(int core) {
        if (topo->isIntel())
            return -1; // was MSR_IA32_PERF_STATUS[6] and HID MSR_IA32_PERF_STATUS[7]
        else
            return Helper::getBits(msrFileRead(core, MSR_AMD_COFVID_STATUS), 6, 8); // MSRC001_0071[CurCpuDid]
    }

    int getVid(int core) {
        if (topo->isIntel())
            return Helper::getBits(msrFileRead(core, MSR_IA32_PERF_STATUS), 0, 7);
        else
            return Helper::getBits(msrFileRead(core, MSR_AMD_COFVID_STATUS), 9, 15); // MSRC001_0071[CurCpuVid] set by lowest P-state of all cores, lower number means higher voltage
    }

    int getFid(int core, int pState) {
        if (!topo->isIntel())
            return Helper::getBits(msrFileRead(core, MSR_AMD_PSTATE_DEF_BASE + pState), 0, 5); // MSRC001_00[6B:64][CpuFid]
        else
            return -1;
    }

    int getDid(int core, int pState) {
        if (!topo->isIntel())
            return Helper::getBits(msrFileRead(core, MSR_AMD_PSTATE_DEF_BASE + pState), 6, 8); // MSRC001_00[6B:64][CpuDid]
        else
            return -1;
    }

    int getVid(int core, int pState) {
        if (!topo->isIntel())
            return Helper::getBits(msrFileRead(core, MSR_AMD_PSTATE_DEF_BASE + pState), 9, 15); // MSRC001_00[6B:64][CpuVid]
        else
            return -1;
    }

    void setVid(int core, int pState, int vid) {
        if (topo->isAMD())
            msrFileWrite(core, MSR_AMD_PSTATE_DEF_BASE + pState, Helper::setBits(msrFileRead(core, MSR_AMD_PSTATE_DEF_BASE + pState), 9, 15, vid)); // MSRC001_00[6B:64][CpuVid]
    }

    int32_t getEnergy(int core) {
        if (topo->isIntel())
            return Helper::getBits(msrFileRead(core, MSR_PKG_ENERGY_STATUS), 0, 31); // 0x00000611, 'wraparound time of around 60 secs when power consumption is high'
        else
            return -1;
    }

    float getEnergyUnit(int core) {
        if (topo->isIntel())
            return 1.0 / (1 << Helper::getBits(msrFileRead(core, MSR_RAPL_POWER_UNIT), 8, 12)); // 0x00000606, 1 / 2 ^ Value
        else
            return 0.0;
    }

    uint64_t tscPState(int pState) { return trbIoctlSet(TURBO_IOC_TSC_PSTATE, pState); }

    bool waitPState(int pState, uint64_t* tsc = nullptr) {
        uint64_t arg = static_cast<unsigned int>(pState);
        int res = ioctl(drvFd, TURBO_IOC_WAIT_PSTATE, &arg);
        Debug::checkErrorCode("MSR:waitPState:ioctl", res);
        if (tsc != nullptr)
            *tsc = arg;
        return (res < TURBO_MAX_WAIT);
    }

    void dummy() { trbIoctlGet(TURBO_IOC_DUMMY); }
};

}

#endif /* JOQUER_MSR_H_ */
