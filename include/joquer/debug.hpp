/* Copyright (c) 2014 Jons-Tobias Wamhoff

 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

#ifndef JOQUER_DEBUG_H_
#define JOQUER_DEBUG_H_

#include <cerrno>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <execinfo.h>

namespace joquer {

class Debug {

    static void printBacktrace()
    {
        void *frames[128];
        size_t num = backtrace(frames, sizeof(frames) / sizeof(frames[0]));
        char **strings = backtrace_symbols(frames, num);

        printf("Obtained %zu stack frames:\n", num);
        size_t i;
        for (i = 0; i < num; i++)
            std::cerr << strings[i] << std::endl;

        free(strings);
    }

    typedef enum {
        RESET     = 0,
        BRIGHT    = 1,
        DIM       = 2,
        UNDERLINE = 3,
        BLINK     = 4,
        REVERSE   = 7,
        HIDDEN    = 8
    } TextFormat;

    typedef enum {
        TC_BLACK     = 0,
        TC_RED       = 1,
        TC_GREEN     = 2,
        TC_YELLOW    = 3,
        TC_BLUE      = 4,
        TC_MAGENTA   = 5,
        TC_CYAN      = 6,
        TC_WHITE     = 7
    } TextColor;

    static inline void textcolor(TextFormat attr = RESET, TextColor fg = TC_WHITE, TextColor bg = TC_BLACK, FILE* fs = stderr) {
        char command[13];
        snprintf(command, sizeof(command), "%c[%d;%d;%dm", 0x1B, attr, fg + 30, bg + 40);
        fprintf(fs, "%s", command);
    }
    static inline void textreset(FILE* fs = stderr) {
        // \e = 033 = 0x1B = 27
        fprintf(fs, "\e[0m");
    }
public:
    static inline void fatalError() __attribute__((noreturn,always_inline)) {
        printBacktrace();
        ::abort();
    }

    static inline void userWarning(const char* msg) __attribute__((always_inline)) {
        textcolor(BRIGHT, TC_YELLOW, TC_BLACK);
        std::cerr << msg << std::endl;
        textreset();
    }

    static inline void userError(const char* msg) __attribute__((noreturn,always_inline)) {
        textcolor(BRIGHT, TC_MAGENTA, TC_BLACK);
        std::cerr << msg << std::endl;
        textreset();
        fatalError();
    }

    static inline bool userCheck(const char* msg, bool condition) __attribute__((always_inline)) {
        if (!condition)
            userError(msg);
        return condition;
    }

    static inline bool checkReturnCode(const char* msg, int rc, int expect = 0) __attribute__((always_inline)) {
        if (rc != expect) {
            textcolor(BRIGHT, TC_RED, TC_BLACK);
            std::cerr << msg << ": " << strerror(rc);
            textreset();
            fatalError();
            return false;
        } else
            return true;
    }

    static inline bool checkErrorCode(const std::string msg, int rc, int fail = -1) __attribute__((always_inline)) {
        return checkErrorCode(msg.c_str(), rc, fail);
    }

    static inline bool checkErrorCode(const char* msg, int rc, int fail = -1) __attribute__((always_inline)) {
        if (rc == fail) {
            textcolor(BRIGHT, TC_RED, TC_BLACK);
            perror(msg);
            textreset();
            fatalError();
            return false;
        } else
            return true;
    }

};

}

#endif /* JOQUER_DEBUG_H_ */
