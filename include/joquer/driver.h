/* Copyright (c) 2014 Jons-Tobias Wamhoff

 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

#ifndef JOQUER_DRIVER_H_
#define JOQUER_DRIVER_H_

#include <linux/ioctl.h>

#define TURBO_IOC_GET_MPERF   _IOR ('t', 0xf0, uint64_t)
#define TURBO_IOC_GET_APERF   _IOR ('t', 0xf1, uint64_t)
#define TURBO_IOC_GET_PSTATE  _IO  ('t', 0xf2)
#define TURBO_IOC_SET_PSTATE  _IO  ('t', 0xf3)
#define TURBO_IOC_TSC_PSTATE  _IOWR('t', 0xf4, uint64_t)
#define TURBO_IOC_WAIT_PSTATE _IOWR('t', 0xf5, uint64_t)
#define TURBO_IOC_DUMMY       _IO  ('t', 0xff)
#define TURBO_IOC_MWAIT _IOWR('t', 0xf6, uint64_t)
#define TURBO_IOC_MSPIN _IOWR('t', 0xf7, uint64_t)

#define TURBO_MAX_WAIT        9000

#endif /* JOQUER_DRIVER_H_ */
