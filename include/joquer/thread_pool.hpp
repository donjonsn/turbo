/* Copyright (c) 2014 Jons-Tobias Wamhoff

 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

#ifndef JOQUER_THREAD_POOL_H_
#define JOQUER_THREAD_POOL_H_

#include <atomic>
#include <memory>
#include <joquer/barrier.hpp>
#include <joquer/debug.hpp>
#include <joquer/thread.hpp>
#include <joquer/topology.hpp>
#include <joquer/thread_storage.hpp>

namespace joquer {

class ThreadPool {
private:
    std::shared_ptr<Topology> topo;
    std::unique_ptr<Barrier> barrier;
    std::unique_ptr<CountBarrier> workerBarrier;
    ThreadStorage<Thread> thread;
    std::vector<std::unique_ptr<Thread> > threads;
    std::atomic<unsigned int> threadCount;
    bool isShutdown;
public:
    ThreadPool(std::shared_ptr<Topology> t = std::make_shared<Topology>()) : topo(t), threadCount(0), isShutdown(false) {}

    ~ThreadPool() {
        isShutdown = true;
        for (auto& t : threads)
            if (t->getWorkFptr() != nullptr)
                t->join(); // wait for finish
        threadCount = 0;
    }

    void create(unsigned int numThreads = 0, Thread::workFunc startFunc = nullptr, void* startArg = 0) {
        assert(threadCount == 0);

        numThreads = numThreads == 0 ? topo->getCoreCount() : numThreads;
        barrier = std::unique_ptr<Barrier>(new Barrier(numThreads + 1)); // TODO make resizable
        workerBarrier = std::unique_ptr<CountBarrier>(new CountBarrier(numThreads)); // TODO make resizable

        for (unsigned int i = 0; i < numThreads; i++) {
            std::unique_ptr<Thread> t(new Thread(threadCount++));
            t->create(startFunc, startArg);
            threads.push_back(std::move(t));
        }
        barrier->wait();
    }

    void record(Thread::workFunc startFunc = nullptr, void* startArg = 0) { // register calling thread
        Thread* t = new Thread(threadCount++);
        setThread(t);
        threads.emplace_back(t);
        t->record(startFunc, startArg); // does not return if startFunc set
    }

    void migrate() { // TODO add parameters for individual threads and affinity policy
        for (auto& t : threads)
            t->migrate(topo->coreMap(t->getThreadId()));
    }

    Barrier* getBarrier() { return barrier.get(); }

    CountBarrier* getWorkerBarrier() { return workerBarrier.get(); }

    unsigned int getThreadCount() { return threadCount; }

    void setThread(Thread* t) { thread.set(t); }

    Thread* getThread() { return thread.get(); }

    bool hasShutdown() { return isShutdown; }
};

}

#endif /* THREAD_POOL_H_ */
