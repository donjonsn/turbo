/* Copyright (c) 2014 Jons-Tobias Wamhoff

 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

#ifndef JOQUER_SYSFS_H_
#define JOQUER_SYSFS_H_

/*
 * CPUfreq Intel: http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/tree/drivers/cpufreq/intel_pstate.c
 * CPUfreq AMD  : http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/tree/drivers/cpufreq/acpi-cpufreq.c
 * ACPI Driver  : http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/tree/drivers/acpi/sysfs.c
 * Kernel Doc   : https://www.kernel.org/doc/Documentation/ABI/testing/sysfs-devices-system-cpu
 */

#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <unistd.h>
#include <joquer/debug.hpp>
#include <joquer/helper.hpp>

namespace joquer {

class SysFSBase {
protected:
    static std::string readSysCpuFile(const std::string path) {
        std::ostringstream fileContent;
        std::ifstream sysFile(path.c_str());
        Debug::checkErrorCode("SysFS:readSysCpuFile:ifstream " + path, sysFile.good(), 0);
        
        fileContent << sysFile.rdbuf();
        sysFile.close();

        std::string content = fileContent.str();

        if (!content.empty() && (content[content.length() - 1] == '\n' || content[content.length() - 1] == '\0'))
            content.erase(content.length() - 1, 1);

        return content;
    }

    static void writeSysCpuFile(const std::string path, const std::string content) {
        std::ostringstream fileContent;
        std::ofstream sysFile(path.c_str());
        Debug::checkErrorCode("SysFS:writeSysCpuFile:ofstream " + path, sysFile.good(), 0);
        
        sysFile.write(content.c_str(), content.length());
        sysFile.close();
    }

    static uint32_t readSysPciFile(const std::string path, int offset) {
        uint32_t fileContent;
        std::ifstream sysFile(path.c_str(), std::ios::out|std::ios::binary);
        Debug::checkErrorCode("SysFS:readSysPciFile:ifstream " + path, sysFile.good(), 0);
        
        sysFile.seekg(offset, std::ios::beg);
        sysFile.read(reinterpret_cast<char*>(&fileContent), sizeof(fileContent) / sizeof(char));
        sysFile.close();

        return fileContent;
    }

    static void writeSysPciFile(const std::string path, int offset, const uint32_t content) {
        std::ofstream sysFile(path.c_str(), std::ios::in|std::ios::binary);
        Debug::checkErrorCode("SysFS:writeSysPciFile:ifstream " + path, sysFile.good(), 0);
        
        sysFile.seekp(offset, std::ios::beg);
        sysFile.write(reinterpret_cast<const char*>(&content), sizeof(content) / sizeof(char));
        sysFile.close();
    }

    static std::string getCpuPath(int core, const std::string suffix) {
        std::stringstream path;
        path << "/sys/devices/system/cpu/cpu" << core << "/" << suffix;
        return path.str();
    }

    static std::string getCpuBasePath(const std::string suffix) {
        std::stringstream path;
        path << "/sys/devices/system/cpu/" << suffix;
        return path.str();
    }

    static std::string getPciPath(int bus, int device, int function) {
        std::stringstream path;
        path << "/sys/bus/pci/devices/0000:" << std::setfill('0') << std::setw(2) << bus << ":" << std::setw(2) << std::hex << device << "." << function << "/config";
        return path.str();
    }

public:
    static std::vector<int> getCoresOnline() { return Helper::parseSequence(readSysCpuFile("/sys/devices/system/cpu/online")); }
    static int getCoreId(int core) { return Helper::parseSequence(readSysCpuFile(getCpuPath(core, "topology/core_id")))[0]; }
    static int getPackageId(int core) { return Helper::parseSequence(readSysCpuFile(getCpuPath(core, "topology/physical_package_id")))[0]; }
    static std::vector<int> getCoreSiblings(int core) { return Helper::parseSequence(readSysCpuFile(getCpuPath(core, "topology/core_siblings_list"))); }
    static std::vector<int> getThreadSiblings(int core) { return Helper::parseSequence(readSysCpuFile(getCpuPath(core, "topology/thread_siblings_list"))); }

    static int getCoresMaxId() { return Helper::parseSequence(readSysCpuFile("/sys/devices/system/cpu/possible")).back(); } // FIXME remove
};

class SysFSCpuFreq : public SysFSBase {
    static std::string getGovernorPath(const std::string suffix) {
        std::stringstream path;
        path << "/sys/devices/system/cpu/cpufreq/" << getGovernor(0) << "/" << suffix;
        return path.str();
    }
public:
    static void checkPrerequisites() {
        if (!Helper::parseSequence(readSysCpuFile(getCpuPath(0, "cpufreq/cpb")))[0])
            Debug::userError("SysFSCpuFreq:checkPrerequisites:noCPB");
    }
    static bool governorAvailable() { return access(getCpuPath(0, "cpufreq/scaling_governor").c_str(), F_OK) != -1; }
    static int getCpuCurrentFrequency(int core) { return Helper::parseSequence(readSysCpuFile(getCpuPath(core, "cpufreq/cpuinfo_cur_freq")))[0]; }
    static int getCpuHardwareLimitMin(int core) { return Helper::parseSequence(readSysCpuFile(getCpuPath(core, "cpufreq/cpuinfo_min_freq")))[0]; }
    static int getCpuHardwareLimitMax(int core) { return Helper::parseSequence(readSysCpuFile(getCpuPath(core, "cpufreq/cpuinfo_max_freq")))[0]; }
    static int getTransitionLatency(int core) { return Helper::parseSequence(readSysCpuFile(getCpuPath(core, "cpufreq/cpuinfo_transition_latency")))[0]; } // in nano seconds
    static int getCorePerformanceBoost(int core) { return Helper::parseSequence(readSysCpuFile(getCpuPath(core, "cpufreq/cpb")))[0]; }
    static int getScalingFreqMin(int core) { return Helper::parseSequence(readSysCpuFile(getCpuPath(core, "cpufreq/scaling_min_freq")))[0]; }
    static int getScalingFreqMax(int core) { return Helper::parseSequence(readSysCpuFile(getCpuPath(core, "cpufreq/scaling_max_freq")))[0]; }
    static int getScalingFreqCur(int core) { return Helper::parseSequence(readSysCpuFile(getCpuPath(core, "cpufreq/scaling_cur_freq")))[0]; }
    static std::string getGovernor(int core) { return readSysCpuFile(getCpuPath(core, "cpufreq/scaling_governor")); }
    static std::string getDriver(int core) { return readSysCpuFile(getCpuPath(core, "cpufreq/scaling_driver")); }
    static std::vector<std::string> getAvailableGovernors(int core) { return Helper::parseList(readSysCpuFile(getCpuPath(core, "cpufreq/scaling_available_governors"))); }
    static std::vector<std::string> getAvailableFrequencies(int core) { return Helper::parseList(readSysCpuFile(getCpuPath(core, "cpufreq/scaling_available_frequencies"))); }
    static int getTotalTransitions(int core) { return Helper::parseSequence(readSysCpuFile(getCpuPath(core, "cpufreq/stats/total_trans")))[0]; }
    static std::string getTransitionTable(int core) { return readSysCpuFile(getCpuPath(core, "cpufreq/stats/trans_table")); }
    static std::string getTimeInState(int core) { return readSysCpuFile(getCpuPath(core, "cpufreq/stats/time_in_state")); }
    static void setGovernor(const std::string gov) {
        std::vector<std::string> govs = getAvailableGovernors(0);

        if (std::find(govs.begin(), govs.end(), gov) != govs.end()) {
            for (auto i : getCoresOnline())
                if (gov != getGovernor(i))
                    writeSysCpuFile(getCpuPath(i, "cpufreq/scaling_governor"), gov);
        } else
            Debug::userError("SysFSCpuFreq:setGovernor:unknown");
    }
    static void setGovSamplingRate(unsigned long mult) { writeSysCpuFile(getGovernorPath("sampling_rate"), Helper::numberToString<unsigned long>(getTransitionLatency(0) * mult)); } // in micro seconds
    static void setGovIgnoreNice(int ignore) { writeSysCpuFile(getGovernorPath("ignore_nice_load"), Helper::numberToString<int>(ignore)); }
    static void setGovFreqStep(int step) { writeSysCpuFile(getGovernorPath("freq_step"), Helper::numberToString<int>(step)); }
    static void setGovUpThreshold(int threshold) { writeSysCpuFile(getGovernorPath("up_threshold"), Helper::numberToString<int>(threshold)); }
    static void setGovDownThreshold(int threshold) { writeSysCpuFile(getGovernorPath("down_threshold"), Helper::numberToString<int>(threshold)); }
    static void setGovSamplingDown(int factor) { writeSysCpuFile(getGovernorPath("sampling_down_factor"), Helper::numberToString<int>(factor)); }
    static void setScalingFreq(int core, int freq) { writeSysCpuFile(getCpuPath(core, "cpufreq/scaling_setspeed"), Helper::numberToString<int>(freq)); }
};

class SysFSIntelPstate : public SysFSBase {
public:
    static bool governorAvailable() { return access(getCpuBasePath("intel_pstate").c_str(), F_OK) != -1; }
    static int getMaxPerfPercent() { return Helper::parseSequence(readSysCpuFile(getCpuBasePath("intel_pstate/max_perf_pct")))[0]; }
    static int getMinPerfPercent() { return Helper::parseSequence(readSysCpuFile(getCpuBasePath("intel_pstate/min_perf_pct")))[0]; }
    static int getNoTurbo() { return Helper::parseSequence(readSysCpuFile(getCpuBasePath("intel_pstate/no_turbo")))[0]; }
    static void setMaxPerfPercent(int pct) { writeSysCpuFile(getCpuBasePath("intel_pstate/max_perf_pct"), Helper::numberToString<int>(pct)); }
    static void setMinPerfPercent(int pct) { writeSysCpuFile(getCpuBasePath("intel_pstate/min_perf_pct"), Helper::numberToString<int>(pct)); }
    static void setNoTurbo(int trb) { writeSysCpuFile(getCpuBasePath("intel_pstate/no_turbo"), Helper::numberToString<int>(trb)); }
};

class SysFSAmd15h : public SysFSBase {
public:
    static void checkPrerequisites() {
        uint32_t ids = readSysPciFile(getPciPath(0, 0x18, 3), 0x0); // D18F3x00[DeviceID|VendorID]
        if (Helper::getBits(ids, 0, 15) != 0x1022 || Helper::getBits(ids, 16, 31) != 0x1603)
            Debug::userError("SysFSAmd15h:checkPrerequisites:noAMDNorthBridge");
    }

    static int getPStatesLock() { return Helper::getBits(readSysPciFile(getPciPath(0, 0x18, 4), 0x15c), 31, 31); } // D18F4x15C[BoostLock]
    static int getPStatesMasterEnable() { return Helper::getBits(readSysPciFile(getPciPath(0, 0x18, 4), 0x15c), 7, 7); } // D18F4x15C[ApmMasterEn]
    static int getPStatesBoosted() { return Helper::getBits(readSysPciFile(getPciPath(0, 0x18, 4), 0x15c), 2, 4); } // D18F4x15C[NumBoostStates]
    static int getPStatesBoostSrc() { return Helper::getBits(readSysPciFile(getPciPath(0, 0x18, 4), 0x15c), 0, 1); } // D18F4x15C[BoostSrc]
    static int getPStatesHwMaxVal() { return Helper::getBits(readSysPciFile(getPciPath(0, 0x18, 3), 0xdc), 8, 10); } // D18F3xDC[HwPstateMaxVal]
    static int getPStatesSwMaxVal() { return Helper::getBits(readSysPciFile(getPciPath(0, 0x18, 3), 0x68), 28, 30); } // D18F3x68[SwPstateLimit]
    static int getPStatesSwMaxEnable() { return Helper::getBits(readSysPciFile(getPciPath(0, 0x18, 3), 0x68), 5, 5); } // D18F3x68[SwPstateLimitEn]
    static int getSwNbPstateLoDis() { return Helper::getBits(readSysPciFile(getPciPath(0, 0x18, 5), 0x170), 14, 14); } // D18F5x170[SwNbPstateLoDis]
    static int getNbPstateDisOnP0() { return Helper::getBits(readSysPciFile(getPciPath(0, 0x18, 5), 0x170), 13, 13); } // D18F5x170[NbPstateDisOnP0]
    static int getVSRampSlamTime() { return Helper::getBits(readSysPciFile(getPciPath(0, 0x18, 3), 0xd8), 4, 6); } // D18F3xD8[VSRampSlamTime]
    static int getPllLockTime() { return Helper::getBits(readSysPciFile(getPciPath(0, 0x18, 3), 0xa0), 11, 12); } // D18F3xA0[PllLockTime]
    static int getSviHighFreqSel() { return Helper::getBits(readSysPciFile(getPciPath(0, 0x18, 3), 0xa0), 9, 9); } // D18F3xA0[SviHighFreqSel]

    static void setSwNbPstateLoDis(int disable) {
        writeSysPciFile(getPciPath(0, 0x18, 5), 0x170, Helper::setBits(readSysPciFile(getPciPath(0, 0x18, 5), 0x170), 14, 14, disable)); // D18F5x170[SwNbPstateLoDis]
    }

    static void setVSRampSlamTime(int factor) {
        writeSysPciFile(getPciPath(0, 0x18, 3), 0xd8, Helper::setBits(readSysPciFile(getPciPath(0, 0x18, 3), 0xd8), 4, 6, factor)); // D18F3xD8[VSRampSlamTime]
    }

    static void setPStatesMasterEnable(int enable) {
        writeSysPciFile(getPciPath(0, 0x18, 4), 0x15c, Helper::setBits(readSysPciFile(getPciPath(0, 0x18, 4), 0x15c), 7, 7, enable)); // D18F4x15C[ApmMasterEn]
    }

    static void setNumBoostedStates(int boostedStates) {
        int enable = getPStatesMasterEnable();
        if (enable)
            setPStatesMasterEnable(0);
        writeSysPciFile(getPciPath(0, 0x18, 4), 0x15c, Helper::setBits(readSysPciFile(getPciPath(0, 0x18, 4), 0x15c), 2, 4, boostedStates)); // D18F4x15C[NumBoostStates]
        if (enable)
            setPStatesMasterEnable(enable);
    }

    static void setPStatesBoostSrc(int src) {
        writeSysPciFile(getPciPath(0, 0x18, 4), 0x15c, Helper::setBits(readSysPciFile(getPciPath(0, 0x18, 4), 0x15c), 0, 1, src)); // D18F4x15C[BoostSrc]
    }

    static unsigned int getMaxProcessorTDP() { return Helper::getBits(readSysPciFile(getPciPath(0, 0x18, 4), 0x1b8), 0, 15); } // D18F4x1B8[ProcessorTdp]
    static unsigned int getMaxNonCoreTDP() { return Helper::getBits(readSysPciFile(getPciPath(0, 0x18, 4), 0x1b8), 16, 31); } // D18F4x1B8[BaseTdp]
    static unsigned int getMaxTotalCoresTDP() {
        int total = Helper::getBits(readSysPciFile(getPciPath(0, 0x18, 4), 0x10c), 0, 11); // D18F4x10C[NodeTdpLimit]
        total *= (Helper::getBits(readSysPciFile(getPciPath(0, 0x18, 3), 0xe8), 29, 29) + 1); // D18F3xE8[MultiNodeCpu]
        return total;
    }

    static unsigned int getCurrentProcessorTDP() { // BKDG MSRC001_0077 Processor Power in TDP
        unsigned int tdp = Helper::getBits(readSysPciFile(getPciPath(0, 0x18, 5), 0xe8), 16, 28); // D18F5xE8[ApmTdpLimit]
        tdp -= static_cast<int>(((Helper::getBits(readSysPciFile(getPciPath(0, 0x18, 5), 0xe0), 4, 25) / (2 << Helper::getBits(readSysPciFile(getPciPath(0, 0x18, 5), 0xe0), 0, 3))) << 10) >> 10); // (signed)(D18F5xE0[TdpRunAvgAccCap] / (2^(D18F5xE0[RunAvgRange] + 1)))
        tdp += getMaxNonCoreTDP();
        return tdp;
    }

    static unsigned int getTDP2Watt(uint64_t tdp) { // mW BKDG MSRC001_0077 Processor Power in TDP
        unsigned int val = readSysPciFile(getPciPath(0, 0x18, 5), 0xe8); // D18F5xE8[Tdp2Watt]
        tdp *= ((val & 0x3ff) << 6) | ((val >> 10) & 0x3f); // http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/tree/drivers/hwmon/fam15h_power.c
        return ((tdp * 15625) >> 10) / 1000;
    }
};

}

#endif /* JOQUER_SYSFS_H_ */
