/* Copyright (c) 2014 Jons-Tobias Wamhoff

 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

#ifndef JOQUER_BARRIER_H_
#define JOQUER_BARRIER_H_

#include <atomic>

#include <joquer/condition.hpp>
#include <joquer/debug.hpp>

namespace joquer {

class Barrier {
private:
    pthread_barrier_t barrier;
public:
    Barrier(unsigned int count) {
        int rc = pthread_barrier_init(&barrier, nullptr, count);
        Debug::checkReturnCode("Barrier:Barrier:init", rc);
    }

    ~Barrier() {
        int rc = pthread_barrier_destroy(&barrier);
        Debug::checkReturnCode("Barrier:Barrier:destroy", rc);
    }

    bool wait() {
        int rc = pthread_barrier_wait(&barrier);
        if (rc == PTHREAD_BARRIER_SERIAL_THREAD)
            return true;
        else {
            Debug::checkReturnCode("Barrier:wait", rc);
            return false;
        }
    }
};

class PortableBarrier {
private:
    Mutex mutex;
    Condition condition;
    unsigned int totalCount;
    unsigned int currentCount;
public:
    PortableBarrier(unsigned int count) : mutex(), condition(&mutex), totalCount(count), currentCount(0) {}

    void wait() {
        mutex.lock();
        currentCount++;
        if (currentCount < totalCount)
            condition.wait();
        else {
            condition.broadcast();
            currentCount = 0;
        }
        mutex.unlock();
    }

    void setNumberParticipants(unsigned int totalCount) { this->totalCount = totalCount; }
    unsigned int getNumberParticipants() { return totalCount; }
};

class CountBarrier {
private:
    unsigned int totalCount;
    std::atomic<unsigned int> currentCount;
    std::atomic<unsigned int> crossFlag;
public:
    CountBarrier(unsigned int count) : totalCount(count), currentCount(0), crossFlag(0) {}

    bool wait() {
        unsigned int go = crossFlag.load(std::memory_order_acquire);
        if (currentCount++ == totalCount - 1) {
            return true;
        } else {
            while (go == crossFlag.load(std::memory_order_acquire)) { Hardware::pause(); }
            return false;
        }
    }

    void cross() {
        currentCount.store(0, std::memory_order_release);
        crossFlag.store(1 - crossFlag.load(std::memory_order_acquire), std::memory_order_release);
    }
};

}

#endif /* JOQUER_BARRIER_H_ */
