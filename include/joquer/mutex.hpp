/* Copyright (c) 2014 Jons-Tobias Wamhoff

 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

#ifndef JOQUER_MUTEX_H_
#define JOQUER_MUTEX_H_

#include <cassert>
#include <climits>
#include <errno.h>
#include <linux/futex.h>
#include <sys/syscall.h>
#include <unistd.h>

#include <joquer/debug.hpp>
#include <joquer/hardware.hpp>
#include <joquer/thread_storage.hpp>

namespace joquer {

class Mutex {
private:
    pthread_mutex_t mutex;
    pthread_mutexattr_t attribute;
public:
    Mutex(int attributeType = PTHREAD_MUTEX_RECURSIVE) {
        int rc;
        rc = pthread_mutexattr_init(&attribute);
        Debug::checkReturnCode("Mutex:Mutex:mai", rc);
        rc = pthread_mutexattr_settype(&attribute, attributeType);
        Debug::checkReturnCode("Mutex:Mutex:mas", rc);
        rc = pthread_mutex_init(&mutex, &attribute);
        Debug::checkReturnCode("Mutex:Mutex:mi", rc);
    }

    ~Mutex() {
        int rc;
        rc = pthread_mutexattr_destroy(&attribute);
        Debug::checkReturnCode("Mutex:Mutex:mad", rc);
        rc = pthread_mutex_destroy(&mutex);
        Debug::checkReturnCode("Mutex:Mutex:md", rc);
    }

    void lock() {
        int rc = pthread_mutex_lock(&mutex);
        Debug::checkReturnCode("Mutex:lock", rc);
    }

    bool tryLock() {
        int rc = pthread_mutex_trylock(&mutex);
        if (rc == EBUSY)
            return false;
        else
            return Debug::checkReturnCode("Mutex:tryLock", rc);
    }

    void unlock() {
        int rc = pthread_mutex_unlock(&mutex);
        Debug::checkReturnCode("Mutex:unlock", rc);
    }

    pthread_mutex_t* getHandle() { return &mutex; }
};

class SpinLock {
private:
    pthread_spinlock_t spinlock;
public:
    SpinLock(int attribute = PTHREAD_PROCESS_PRIVATE) {
        int rc = pthread_spin_init(&spinlock, attribute);
        Debug::checkReturnCode("SpinLock:SpinLock:init", rc);
    }

    ~SpinLock() {
        int rc = pthread_spin_destroy(&spinlock);
        Debug::checkReturnCode("SpinLock:SpinLock:destroy", rc);
    }

    void lock() {
        int rc = pthread_spin_lock(&spinlock);
        Debug::checkReturnCode("SpinLock:lock", rc);
    }

    bool tryLock() {
        int rc = pthread_spin_trylock(&spinlock);
        if (rc == EBUSY)
            return false;
        else
            return Debug::checkReturnCode("SpinLock:tryLock", rc);
    }

    void unlock() {
        int rc = pthread_spin_unlock(&spinlock);
        Debug::checkReturnCode("SpinLock:unlock", rc);
    }

    pthread_spinlock_t* getHandle() { return &spinlock; }
};

/*
 * http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/tree/include/linux/futex.h
 * http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/tree/kernel/futex.c
 */

class Futex {
private:
    int f;

    static int sysFutex(void* addr1, int op, int val1, struct timespec* timeout = nullptr, void* addr2 = nullptr, int val3 = 0) {
	    return syscall(SYS_futex, addr1, op, val1, timeout, addr2, val3);
    }
public:
    Futex() : f(0) {}

    void lock(int* ptr, int val = 0) { // val must match ptr
        int rc = sysFutex(ptr, FUTEX_WAIT_PRIVATE, val);
        if (rc == -1 && errno != EWOULDBLOCK && errno != EINTR && errno != ETIMEDOUT) // val changed (11 == EAGAIN) || signal (4) || timeout (110)
            Debug::checkErrorCode("Futex:lock", rc);
    }
    void lock(int val = 0) { lock(&f, val); }

    void unlock(int* ptr, int numWakeThreads = INT_MAX) { sysFutex(ptr, FUTEX_WAKE_PRIVATE, numWakeThreads); } // return number of woken threads
    void unlock(int numWakeThreads = INT_MAX) { unlock(&f, numWakeThreads); }
};

class ClhLock {
private:
    class ClhNode {
    public:
        /// True indicates that the transaction is waiting for a lock or acquired it.
        /// False indicates that the lock has been released.
        bool locked;
        ClhNode* pred;
    };
    // Current tail of CLH lock queue.
    ClhNode* tail;
    ThreadStorage<ClhNode> node;
public:
    ClhLock() {
        tail = new ClhNode();
        tail->locked = false;
    }

    void lock() {
        ClhNode* myNode = node.get();
        if (myNode == nullptr) {
            myNode = new ClhNode();
            node.set(myNode);
        }
        myNode->locked = true;
        myNode->pred = __sync_lock_test_and_set(&tail, myNode);
        // Spin until predecessor releases lock.
        while ((volatile ClhNode*)(myNode->pred)->locked) {
            Hardware::pause();
        }
        assert(!myNode->pred->locked);
    }

    void unlock() {
        ClhNode* myNode = node.get();
        myNode->locked = false;
        node.set(myNode->pred);
    }
};

}

#endif /* JOQUER_MUTEX_H_ */
