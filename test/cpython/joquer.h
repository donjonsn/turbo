/* Copyright (c) 2014 Jons-Tobias Wamhoff

 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

#include <pthread.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef enum trb_thread_type {
    TRB_MAIN = 0,
    TRB_WORKER,
    TRB_CLIENT
} trb_thread_type_t;

typedef enum trb_event_type {
    TRB_ACQUIRE = 0,
    TRB_OWN,
    TRB_RELEASE,
    TRB_EVENT_MAX,
    TRB_WAIT,
    TRB_START,
    TRB_STOP,
    TRB_MARK_CLIENT,
    TRB_EXIST_CLIENT
} trb_event_type_t;

void trb_init_thread(trb_thread_type_t source);

void trb_exit_thread(trb_thread_type_t source);

void trb_event(trb_event_type_t event);

#ifdef __cplusplus
}
#endif
