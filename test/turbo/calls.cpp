/* Copyright (c) 2014 Jons-Tobias Wamhoff

 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

#include <joquer.hpp>

enum calls {
    SYSCALL_GETPID = 0,
    TURBO_DUMMY,
    MSR_MPERF,
    TURBO_MPERF,
    MSR_GET_PSTATE,
    TURBO_GET_PSTATE,
    MSR_SET_PSTATE,
    TURBO_SET_PSTATE,
    TURBO_TSC_PSTATE,
    TURBO_WAIT_PSTATE,
    MSR_SIBLING,
    MONITOR_MWAIT,
    SET_AFFINITY,
    FOR_LOOP,
    SYSCALL_FUTEX_WAIT,
    SYSCALL_FUTEX_WAKE,
    CALLS_MAX
};

static const char* token[] = {"SYSCALL_GETPID      ",
                              "TURBO_DUMMY         ",
                              "MSR_MPERF           ",
                              "TURBO_MPERF         ",
                              "MSR_GET_PSTATE      ",
                              "TURBO_GET_PSTATE    ",
                              "MSR_SET_PSTATE      ",
                              "TURBO_SET_PSTATE    ",
                              "TURBO_TSC_PSTATE    ",
                              "TURBO_WAIT_PSTATE   ",
                              "MSR_SIBLING         ",
                              "MONITOR_MWAIT       ",
                              "SET_AFFINITY        ",
                              "FOR_LOOP            ",
                              "SYSCALL_FUTEX_WAIT  ",
                              "SYSCALL_FUTEX_WAKE  "};

static volatile unsigned long long __attribute__ ((aligned (64))) th_cnt[8] = {0};
static void th_execute(void* arg, Thread* t) {
    t->migrate(reinterpret_cast<long>(arg));
    while(true) { th_cnt[0]++; }
}

static void th_loop(uint64_t iterations) __attribute__((noinline,optimize(0)));
static void th_loop(uint64_t iterations) {
    for (uint64_t i = 0; i < iterations; i++) {}
    asm ("");
}

int main(int argc, char * argv[]) {
    std::shared_ptr<Topology> topo = std::make_shared<Topology>();
    MSR msr(topo); // FIXME use PState class
    Thread thSelf(0);
    thSelf.record();
    Thread thSibling(1);
    Futex futex;
    int pState = 0;
    unsigned int totalIterations = 1000;
    int coreId = thSelf.getCoreId();
    unsigned int callStart = 0;
    unsigned int callEnd = CALLS_MAX;
    uint64_t loopIterations = 1000;

    if (argc > 1)
        pState = Helper::stringToNumber<int>(argv[1]);
    if (argc > 2)
        totalIterations = Helper::stringToNumber<unsigned int>(argv[2]);
    if (argc > 3)
        coreId = Helper::stringToNumber<int>(argv[3]);
    if (argc > 4) {
        callStart = Helper::stringToNumber<unsigned int>(argv[4]);
        callEnd = callStart + 1;
    }
    if (argc > 5)
        loopIterations = (1000 * Helper::stringToNumber<uint64_t>(argv[5])) / 10;
    
    int pInit = msr.getPState(coreId);
    uint64_t* samples = static_cast<uint64_t*>(malloc(totalIterations * sizeof(uint64_t)));
    thSelf.migrate(coreId);

    std::vector<int> sibling = topo->getThreadSiblings(coreId);
    int siblingId = sibling.at(0) == coreId ? sibling.at(1) : sibling.at(0); // FIXME for Intel

    uint64_t  freq; //MHz
    if (topo->isIntel())
        freq = msr.getPStateBase(0) * 100; // On haswell (TSC invariant), p-state(Max, non-turbo) * 100MHz = Frequency
    else {
        int pBoost = SysFSAmd15h::getPStatesBoosted();
        freq       = (msr.getFid(coreId, pBoost) + 16) * 200 / (2 << msr.getDid(coreId, pBoost)); //MHz
    }

    std::cout << "Switching p-state from " << pInit << " to " << pState << " for " << totalIterations << " iterations on core " << coreId << std::endl;
    std::cout << "                                mean         deviation              min              max " << std::endl;
    std::cout << "                        cycles (   ns)   cycles (   ns)   cycles (   ns)   cycles (   ns)" << std::endl;

    for (unsigned int c = callStart; c < callEnd; c++) {
        uint64_t tscTotal = 0;
        uint64_t tscSquare = 0;
        uint64_t tscMin = 0 - 1;
        uint64_t tscMax = 0;
        bool waitNotExceeded = true;
        unsigned int actualIterations = 0;

        if (c == MONITOR_MWAIT || c == SET_AFFINITY) {
            if (!CPUID::getMonitorFeature()) printf("XXX\n"); // FIXME for Intel
            msr.setMonMwaitUserEnable(coreId);
            thSibling.create(&th_execute, reinterpret_cast<void*>((coreId + 2) % 7));
            while (th_cnt[0] == 0) {}
        }

        for (unsigned int i = 0; i < totalIterations; i++) {
            if (c == MSR_SET_PSTATE || c == TURBO_SET_PSTATE || c == TURBO_TSC_PSTATE || c == TURBO_WAIT_PSTATE) {
                if (pInit < pState)
                    msr.setPState(pState, siblingId);
            } else if (c == MSR_SIBLING) {
                if (pInit < pState)
                    msr.setPState(pState, coreId);
            }
            
            uint64_t tsc = Hardware::readTSC();

            switch (c) {
                case SYSCALL_GETPID:
                    syscall(__NR_getpid);
                    break;
                case SYSCALL_FUTEX_WAIT:
                    futex.lock(1);
                    break;
                case SYSCALL_FUTEX_WAKE:
                    futex.unlock();
                    break;
                case MSR_MPERF:
                    msr.getMPerf(coreId);
                    break;
                case MSR_GET_PSTATE:
                    msr.getPState(coreId);
                    break;
                case MSR_SET_PSTATE:
                    msr.setPState(pState, coreId);
                    break;
                case TURBO_MPERF:
                    msr.getMPerf();
                    break;
                case TURBO_GET_PSTATE:
                    msr.getPState();
                    break;
                case TURBO_SET_PSTATE:
                    msr.setPState(pState);
                    break;
                case TURBO_TSC_PSTATE:
                    tsc = msr.tscPState(pState);
                    break;
                case TURBO_WAIT_PSTATE:
                    waitNotExceeded = msr.waitPState(pState, &tsc);
                    break;
                case MSR_SIBLING:
                    msr.setPState(pState, siblingId);
                    break;
                case TURBO_DUMMY:
                    msr.dummy();
                    break;
                case MONITOR_MWAIT:
                    Hardware::monitor((void*)&th_cnt[0]);
                    if (th_cnt[0])
                        Hardware::mwait();
                    else
                        waitNotExceeded = false;
                    break;
                case SET_AFFINITY:
                    thSelf.migrate((coreId + 1 + (coreId % 2)) % 7);
                    break;
                case FOR_LOOP:
                    th_loop(loopIterations);
                    break;
                default:
                    Debug::userError("unkonwn test call");
                    break;
            }

            if (c != TURBO_TSC_PSTATE && c != TURBO_WAIT_PSTATE)
                tsc = Hardware::readTSC() - tsc;

            if (c == MSR_SET_PSTATE || c == TURBO_SET_PSTATE || c == TURBO_TSC_PSTATE || c == TURBO_WAIT_PSTATE) {
                while (msr.getPState(coreId) != pState) {}

                if (pInit > pState)
                    msr.setPState(pInit, siblingId);
                msr.waitPState(pInit);
            } else if (c == MSR_SIBLING) {
                while (msr.getPState(siblingId) != pState) {}

                msr.setPState(pInit, siblingId);
                if (pInit > pState)
                    msr.waitPState(pInit);
            }

            if (c == SET_AFFINITY)
                thSelf.migrate(coreId);

            if (waitNotExceeded) {
                actualIterations++;
                if (tsc > tscMax) tscMax = tsc;
                if (tsc < tscMin) tscMin = tsc;
                tscTotal += tsc;
                tscSquare += (tsc * tsc);
                samples[i] = tsc;
            } else {
                exit(EXIT_FAILURE);
            }

            if (c == MSR_SET_PSTATE || c == TURBO_SET_PSTATE || c == TURBO_TSC_PSTATE || c == TURBO_WAIT_PSTATE || c == MSR_SIBLING) {
                volatile uint64_t tscWait = Hardware::readTSC() + (freq * 500);
                while (Hardware::readTSC() < tscWait) {}
            }
        }

        if (c == MONITOR_MWAIT || c == SET_AFFINITY)
            thSibling.exit();

        uint64_t  mean    = tscTotal / actualIterations;
        uint64_t  dev     = actualIterations > 1 ? sqrt(abs(tscSquare - (tscTotal * tscTotal) / actualIterations) / (actualIterations - 1)) : 0;

        std::cout << token[c] << " " <<
            std::setw(9) << mean   << " (" << std::setw(5) << mean   * 1000 / freq << ")" <<
            std::setw(9) << dev    << " (" << std::setw(5) << dev    * 1000 / freq << ")" <<
            std::setw(9) << tscMin << " (" << std::setw(5) << tscMin * 1000 / freq << ")" <<
            std::setw(9) << tscMax << " (" << std::setw(5) << tscMax * 1000 / freq << ")";

        //for (unsigned int i = 0; i < totalIterations; i++)
        //    std::cout << " " << samples[i];

        std::cout << std::endl;
    }

    free(samples);
    
    return EXIT_SUCCESS;
}
