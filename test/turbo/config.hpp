/* Copyright (c) 2014 Jons-Tobias Wamhoff

 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

#ifndef TURBO_CONFIG_H_
#define TURBO_CONFIG_H_

#include <argp.h>
#include <string>
#include <unistd.h>
#include <joquer.hpp>

#define GCCDBG 1

#ifndef GCCDBG
# define DBG_CALL(func)
# define DBG_MSG(...)
#else
# define DBG_CALL(func) func
# define DBG_MSG(...) printf(__VA_ARGS__); fflush(NULL)
#endif

namespace turbo {

class Mode {
public:
    enum mode {
       MODE_INFO = 0,
       MODE_CONFIG,
       MODE_NB
    };
};

extern "C" { // named initializers work only in C99
    const char* const mode_opts[Mode::MODE_NB + 1] = {
       [Mode::MODE_INFO]   = "info",
       [Mode::MODE_CONFIG] = "config",
       [Mode::MODE_NB]     = nullptr
    };
}

class Config {
private:
    const char* doc = "Turbo -- a program to play with the p-states\n\n"
        " info      Print CPU information\n"
        " config    Configure CPUs permamently";
    const char* args_doc = "[info|config|switch|bench]";
    struct argp_option options[13] = {
       {NULL,           0,  NULL,    0, "General Parameters: "},
       {"threads",     't', "0",     0, "Number of threads (0=num cores)"},
       {"cpus",        'c', "all",   0, "Apply changes to selected CPUs"},
       {"iterations",  'i', "0",     0, "Number of iterations to execute"},
       {NULL,           0,  NULL,    0, "Configuration parameters: -c"},
       {"pboosted",    'o', "-1",    0, "Number of boosted p-states"},
       {"pstate",      'p', "-1",    0, "Set the p-state"},
       {0}
    };
    static error_t argp_parser(int key, char* arg, struct argp_state* state) {
        Config* c = static_cast<Config*>(state->input);

        switch (key) {
            case 't':
                c->nbThreads = Helper::stringToNumber<int>(arg);
                break;
            case 'c':
                c->selectCpus = std::string(arg);
                break;
            case 'i':
                c->iterations = Helper::stringToNumber<unsigned int>(arg);
                break;
            case 'o':
                c->bState = Helper::stringToNumber<int>(arg);
                break;
            case 'p':
                c->pState = Helper::stringToNumber<int>(arg);
                break;
            case ARGP_KEY_ARG:
                if (state->arg_num >= 1)
                    argp_usage(state);
                char* val;
                c->executionMode = getsubopt(&arg, const_cast<char* const*>(mode_opts), &val);
                Debug::userCheck("Config::argp_parser:unsupported mode", c->executionMode != -1);
                break;
            case ARGP_KEY_END:
                if (state->arg_num < 1)
                    argp_error(state, "no mode specified");
                break;
            default:
                return ARGP_ERR_UNKNOWN;
        }
        return 0;
    }
    struct argp argp = {options, argp_parser, args_doc, doc};
public:
    // Configuration
    int nbThreads;
    int iterations;
    int pState;
    int bState;
    int executionMode;
    std::string selectCpus;

    Config(int argc, char **argv) : nbThreads(0), iterations(0), pState(-1), bState(-1), executionMode(Mode::MODE_NB) {
        selectCpus = Helper::vectorToString<int>(JoQueR::getInstance(true).getTopology()->getCoresOnline(), std::string(","));
        int rc = argp_parse(&argp, argc, argv, 0, 0, this);
        Debug::checkErrorCode("Config:Config:argp_parse", rc);
    }
};

}

#endif /* TURBO_CONFIG_H_ */
